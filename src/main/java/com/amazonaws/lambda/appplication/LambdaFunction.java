package com.amazonaws.lambda.appplication;

import java.util.function.Function;

import org.springframework.stereotype.Component;

import com.amazonaws.lambda.controller.LambdaController;
import com.amazonaws.lambda.domain.Request;
import com.amazonaws.lambda.domain.Response;

@Component("lambdaFunction")
public class LambdaFunction implements Function<Request, Response>{

	private LambdaController lambdaController;
	
	public LambdaFunction(final LambdaController lambdaController) {
		this.lambdaController = lambdaController;
	}
	
	@Override
	public Response apply(Request t) {
		final Response response = new Response();
		response.setSchedule(lambdaController.generateShifts(t.getShiftsPerPeriod(), t.getMaxShiftsPerEngineer()));
		return response;
	}

}
