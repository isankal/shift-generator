package com.amazonaws.lambda.appplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class LambdaFunctionApplication {

	public static void main(String[] args) throws Exception{
		SpringApplication.run(LambdaFunctionApplication.class, args);

	}

}
