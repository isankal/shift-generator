package com.amazonaws.lambda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amazonaws.lambda.models.Engineer;

public interface EngineerRepository extends JpaRepository<Engineer, Long> {

}
