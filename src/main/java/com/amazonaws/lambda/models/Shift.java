package com.amazonaws.lambda.models;

public class Shift {
	
	private int shiftId;
	private Engineer engineer;
	
	public int getShiftId() {
		return shiftId;
	}
	
	public Shift(int shiftId) {
		this.shiftId = shiftId;
	}
	public void setShiftId(int shiftId) {
		this.shiftId = shiftId;
	}
	public Engineer getEngineer() {
		return engineer;
	}
	public void setEngineer(Engineer engineer) {
		this.engineer = engineer;
	}
	
	

}
