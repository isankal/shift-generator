package com.amazonaws.lambda.manager;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.amazonaws.lambda.rules.ConsecutiveDayRule;
import com.amazonaws.lambda.rules.RuleValidator;
import com.amazonaws.lambda.rules.ShiftsPerDayRule;

@Component
public class RulesManager {
	public List<RuleValidator> getRules() {
		List<RuleValidator> rules = new ArrayList<RuleValidator>();
		rules.add(new ConsecutiveDayRule());
		rules.add(new ShiftsPerDayRule());
		return rules;
	}
}
