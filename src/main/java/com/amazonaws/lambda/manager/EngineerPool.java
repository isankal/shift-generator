package com.amazonaws.lambda.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.amazonaws.lambda.models.Engineer;

public class EngineerPool {
	private List<Engineer> engineers;
	private List<Engineer> availableEngineers;
	private Random random;
	
	public EngineerPool() {
		engineers = new ArrayList<Engineer>();
		availableEngineers = new ArrayList<Engineer>();
		random = new Random();
	}
	
	public void addEngineers(List<Engineer> engineers) {
		engineers.addAll(engineers);
		availableEngineers.addAll(engineers);
	}
	
	public void remove(Engineer engineer) {
		engineers.remove(engineer);
    }
	
	public Engineer getRandomEngineer() {
		Engineer engineer = null;
		
		if (!availableEngineers.isEmpty()) {
			engineer = availableEngineers.get(random.nextInt(availableEngineers.size()));
			availableEngineers.remove(engineer);
		} 
		
		return engineer;
	}
}
