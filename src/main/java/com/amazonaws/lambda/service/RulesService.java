package com.amazonaws.lambda.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.amazonaws.lambda.manager.RulesManager;
import com.amazonaws.lambda.models.Shift;
import com.amazonaws.lambda.rules.RuleValidator;


@Service("rulesService")
public class RulesService {

	private RulesManager ruleManger;
	
	public RulesService(RulesManager ruleManger) {
		this.ruleManger = ruleManger;
	}

	public boolean isValid(int shiftId, long employeeId, List<Shift> shifts) {
		boolean valid = true;

		for (RuleValidator rule : ruleManger.getRules()) {
			valid = rule.isValid(shiftId, employeeId, shifts);
			if (!valid) {
				break;
			}
		}

		return valid;
	}
}
