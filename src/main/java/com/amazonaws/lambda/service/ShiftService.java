package com.amazonaws.lambda.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.amazonaws.lambda.manager.EngineerPool;
import com.amazonaws.lambda.models.Engineer;
import com.amazonaws.lambda.models.Shift;

@Service("shiftService")
public class ShiftService {

	private RulesService rulesService;
	
	public ShiftService(RulesService rulesService) {
		this.rulesService = rulesService;
	}

	public ArrayList<Shift> createShifts(EngineerPool engineerPool, int shiftsPerPeriod) {
		ArrayList<Shift> shifts = new ArrayList<Shift>();

		for (int i = 0; i < shiftsPerPeriod; i++) {
			shifts.add(new Shift(i));
		}

		Engineer engineer;
		while ((engineer = engineerPool.getRandomEngineer()) != null) {
			for (int i = 0; i < shiftsPerPeriod; i++) {
				if (shifts.get(i).getEngineer() == null) {
					boolean foundSuitableSlot = rulesService.isValid(i, engineer.getEmpId(), shifts);
					if (foundSuitableSlot) {
						shifts.get(i).setEngineer(engineer);
						engineerPool.remove(engineer);
						break;
					}
				}
			}
		}
		return shifts;
	}
	
}

