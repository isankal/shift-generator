package com.amazonaws.lambda.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.amazonaws.lambda.dao.EngineerRepository;
import com.amazonaws.lambda.manager.EngineerPool;
import com.amazonaws.lambda.models.Schedule;
import com.amazonaws.lambda.models.Shift;

@Service("schedulerService")
public class SchedulerService {

	private ShiftService shiftService;

	private EngineerRepository engineerRepository;

	public SchedulerService(ShiftService shiftService, EngineerRepository engineerRepository) {
		this.shiftService = shiftService;
		this.engineerRepository = engineerRepository;
	}

	public Schedule createSchedule(int shiftsPerPeriod, int maxShiftsPerEngineer) {
		ArrayList<Shift> shifts = new ArrayList<Shift>();

		while (shifts.stream().filter(engineer -> engineer.getEngineer() != null).count() < shiftsPerPeriod) {
			EngineerPool engineerPool = createEngineerPool(maxShiftsPerEngineer);
			shifts = shiftService.createShifts(engineerPool, shiftsPerPeriod);
		}

		return new Schedule(shifts);
	}

	private EngineerPool createEngineerPool(int maxShiftsPerEngineer) {
		EngineerPool engineerPool = new EngineerPool();

		for (int i = 0; i < maxShiftsPerEngineer; i++) {
			engineerPool.addEngineers(engineerRepository.findAll());
		}

		return engineerPool;
	}
}
