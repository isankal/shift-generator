package com.amazonaws.lambda.domain;

public class Request {
	
	private int shiftsPerPeriod;
    
    private int maxShiftsPerEngineer;

	public int getShiftsPerPeriod() {
		return shiftsPerPeriod;
	}

	public void setShiftsPerPeriod(int shiftsPerPeriod) {
		this.shiftsPerPeriod = shiftsPerPeriod;
	}

	public int getMaxShiftsPerEngineer() {
		return maxShiftsPerEngineer;
	}

	public void setMaxShiftsPerEngineer(int maxShiftsPerEngineer) {
		this.maxShiftsPerEngineer = maxShiftsPerEngineer;
	}
    
    
}
