package com.amazonaws.lambda.domain;

import com.amazonaws.lambda.models.Schedule;

public class Response {

    private Schedule schedule;

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

   
}