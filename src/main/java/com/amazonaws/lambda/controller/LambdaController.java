package com.amazonaws.lambda.controller;

import java.util.Locale;

import org.springframework.stereotype.Component;

import com.amazonaws.lambda.models.Schedule;
import com.amazonaws.lambda.service.SchedulerService;

@Component
public class LambdaController {

	private SchedulerService schedulerService;
	
	
	public LambdaController(SchedulerService schedulerService) {
		this.schedulerService = schedulerService;
	}

	
	public Schedule generateShifts(int shiftsPerPeriod, int maxShiftsPerEngineer) {
		return schedulerService.createSchedule(shiftsPerPeriod, maxShiftsPerEngineer);
	}

	public String uppercase(final String input) {
        return input.toUpperCase(Locale.ENGLISH);
    }
}
