package com.amazonaws.lambda.rules;

import java.util.List;

import com.amazonaws.lambda.models.Shift;

public interface RuleValidator {
	boolean isValid(int shiftId, long employeeId, List<Shift> shifts);
}